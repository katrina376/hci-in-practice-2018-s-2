from datetime import datetime
from apscheduler.schedulers.blocking import BlockingScheduler


def print_time():
    print('The current time is:', datetime.now())


sched = BlockingScheduler()
sched.add_job(print_time, 'interval', seconds=2)

sched.start()
