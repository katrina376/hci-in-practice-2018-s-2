import requests
from bs4 import BeautifulSoup


resp = requests.get('https://katrina.tw')
resp.encoding = 'UTF-8'
html_doc = resp.text

soup = BeautifulSoup(html_doc, 'html.parser')

a_list = soup.select('a')
for a in a_list:
    print(a.get('href'))
