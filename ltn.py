import requests
from bs4 import BeautifulSoup


resp = requests.get(
    'http://news.ltn.com.tw/search',
    params={
        'keyword': '火災',
        'page': '1',
    },
)

resp.encoding = 'UTF-8'
html_doc = resp.text

soup = BeautifulSoup(html_doc, 'html.parser')

news = soup.select_one('#newslistul').select('li')

for n in news:
    title = n.select_one('.tit').select_one('p').string
    tag = n.select_one('.immtag').string
    time = n.select_one('span').string
    highlight = ''.join(list(n.select('p')[1].strings))

    print('---')
    print('Title:', title)
    print('Category:', tag)
    print('Publish:', time)
    print('Highlight:', highlight)
